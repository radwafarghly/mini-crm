<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employ extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name','email','phone','company_id'];

    /**
     * Get the Company that owns the Employ.
     */
    public function companies()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
}
