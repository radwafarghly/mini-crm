<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','logo'];

     /**
     * Get the Employ for the Company.
     */
    public function employees()
    {
        return $this->hasMany('App\Employ');
    }
}
