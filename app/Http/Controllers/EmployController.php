<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employ;
use App\Company;
use App\Http\Requests\EmployRequest;
use App\http\Requests\EditEmployRequest;

class EmployController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employ::latest()->paginate(10);

        return view('employ.index', compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * 10);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $companies = Company::pluck('name', 'id');

        return view('employ.create',compact('companies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployRequest $request)
    {
        //
        $employ = new Employ;
        $employ->first_name = $request->first_name;
        $employ->last_name = $request->last_name;
        $employ->email = $request->email;
        $employ->phone=$request->phone;
        $employ->company_id = $request->company_id;
        $employ->save();
        return redirect()->route('employees.index')
            ->with('success', 'Employee Added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $employ = Employ::find($id);
        return view('employ.show',compact('employ'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee = Employ::find($id);
        $companies = Company::pluck('name', 'id');
        return view('employ.edit', compact('employee', 'companies'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditEmployRequest $request, $id)
    {
        //
        //dd($request->all());
        $employ = Employ::find($id);
        $employ->first_name = $request->first_name;
        $employ->last_name = $request->last_name;
        $employ->email = $request->email;
        $employ->phone = $request->phone;
        $employ->company_id = $request->company_id;
        $employ->save();
        return redirect()->route('employees.index')
            ->with('success', 'Employee Updated successfully');

    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Employ::find($id)->delete();
        return redirect()->route('employees.index')
          ->with('success', 'Employee deleted successfully');

    }
}
