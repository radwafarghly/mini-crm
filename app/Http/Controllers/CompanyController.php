<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\EditCompanyRequest;
use App\Company;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = Company::latest()->paginate(10);
        return view('company.index', compact('companies'))
            ->with('i', (request()->input('page', 1) - 1) * 10);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
      // dd($request);
        if($request->hasFile('logo')){
            $extension =$request->file("logo")->getClientOriginalExtension();
            
            $fileNameStore=$request->input('name').'_'.time().'.'.$extension;
            
            $path=$request->file("logo")->storeAs('public/logo', $fileNameStore);            
        }
        else{
            $fileNameStore='avatar.jpg';
        }
        $company = new Company;
        $company->name = $request->input('name');
        $company->email = $request->input('email');
        $company->logo = $fileNameStore;
        $company->save();

        Mail::to('admin@admin.com')->send(new SendMail($company));
        return redirect()->route('companies.index')
          ->with('success', 'Company created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('company.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyRequest $request, $id)
    {
       // dd($request);
        $company = Company::find($id);
        if ($request->hasFile('logo')) {
            $extension = $request->file("logo")->getClientOriginalExtension();
            $fileNameStore = $request->input('name') . '_' . time() . '.' . $extension;
            $path = $request->file("logo")->storeAs('public/logo', $fileNameStore);
        }else{
            $fileNameStore=$company->logo;
        }
        //dd($request->all());
        $company = Company::find($id);

        $company->name = $request->name;
        $company->email = $request->email;
        $company->logo = $fileNameStore;
        $company->save();
        return redirect()->route('companies.index')
          ->with('success', 'company updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::find($id)->delete();
        return redirect()->route('companies.index')
            ->with('success', 'company deleted successfully');

    }
}
