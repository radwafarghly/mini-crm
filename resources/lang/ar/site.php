<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'Language'=>'اللغه',
    'Login' =>'تسجيل الدخول',
    'Logout' =>'تسجيل خروج',
    'E-Mail Address'=>'البريد الالكتروني',
    'Password'=>'كلمه السر',
    'Confirm Password'=>'تاكيد كلمه السر',
    'Remember Me'=>'تذكرني',
    'Forgot Your Password?'=>'نسيت كلمه المرور؟',
    'Reset Password'=>'استرجاع كلمه السر',
    'Send Password Reset Link' =>'ارسال رابط استرجاع كلمه السر',
    'welcome in Mini-CRM'=>'اهلا بيك في Mini-CRM',
    'Home'=>'الصفحه الرئيسيه',
     'Add New company'=>'اضافه شركه جديده',
    'Back'=>'للخلف',
    'Name'=>'الاسم',
    'Email'=>'الايميل',
    'Logo'=>'رمز الشركه',
    'Submit'=>'ارسال',
    'Edit'=>'تعديل',
    'Show'=>'عرض',
    'Delete'=>'حذف',
    'Action'=>'Action',
    'No'=>'الرقم',
    'Add New Employ'=>'اضافه موظف جديد',
    'First Name'=>'الاسم الاول',
    'Last Name'=>'الاسم الاخير',
    'Phone'=>'رقم الهاتف',
    'Company'=>'الشركه',
    'Employees'=>'الموظفين',
    'Companies'=>'الشركات',
    'Admin panel'=>'لوحه تحكم',
    'to manage'=>'لادراه',
    'Basically'=>'في الاساس',
    'a project to manage'=>'المشروع لاداره',
    'and'=>'و',
];
