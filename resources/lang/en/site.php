<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */
    'Language'=>'Language',
    'Login'=>'Login',
    'Logout'=>'Logout',
    'E-Mail Address' =>'E-Mail Address',
    'Password'=>'Password',
    'Remember Me'=>'Remember Me',
    'Forgot Your Password?'=>'Forgot Your Password?',
    'Reset Password'=>'Reset Password',
    'Confirm Password'=>'Confirm Password',
    'welcome in Mini-CRM'=>'welcome in Mini-CRM',
    'Home'=>'Home',
    'Add New company'=>'Add New company',
    'Back'=>'Back',
    'Name'=>'Name',
    'Email'=>'Email',
    'Logo'=>'Logo',
    'Submit'=>'Submit',
    'Edit'=>'Edit',
    'Show'=>'Show',
    'Delete'=>'Delete',
    'Action'=>'Action',
    'No'=>'No',
    'Add New Employ'=>'Add New Employee',
    'First Name'=>'First Name',
    'Last Name'=>'Last Name',
    'Phone'=>'Phone',
    'Company'=>'Company',
    'Employees'=>'Employees',
    'Companies'=>'Companies',
    'Admin panel'=>'Admin panel',
    'to manage'=>'to manage',
    'Basically'=>'Basically ',
    'a project to manage'=>' a project to manage',
    'and'=>'and',
];
