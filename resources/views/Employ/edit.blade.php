
@extends('layouts.app')
 
@section('content')

<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
                <h2>{{ __('site.Edit')}}   [{{ $employee->first_name }}  {{ $employee->last_name }} ]</h2>
                <hr>

	        </div>
	    </div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Warning!!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    {!! Form::model($employee, ['method' => 'PATCH','route' => ['employees.update', $employee->id],'enctype'=>'multipart/form-data']) !!}
	<div class="row">
		 <div class="col-xs-12 col-sm-12 col-md-6">
           <div class="form-group">
               <strong>{{ __('site.First Name')}}:</strong>
                {!! Form::text('first_name', null, array('placeholder' => 'First Name','class' => 'form-control')) !!}
           </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Last Name')}}:</strong>
                    {!! Form::text('last_name', null, array('placeholder' => 'Last Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Email')}}:</strong>
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Phone')}}:</strong>
                    {!! Form::text('phone', null, array('placeholder' => 'Phone','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('site.Company')}}</strong>
                    {!! Form::select('company_id', $companies,$employee->company_id, array('class' => 'form-control')) !!}

                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">{{ __('site.Submit')}}</button>
                <a class="btn btn-primary" href="{{ route('employees.index') }}"> {{ __('site.Back')}}</a>

        </div>
	</div>
    {!! Form::close() !!}
</div>
@endsection