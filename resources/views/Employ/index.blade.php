@extends('layouts.app')
 
@section('content')
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>{{ __('site.Employees')}}</h2>
                <hr>
            </div>
            <div class="pull-right p-2">
                <a class="btn btn-success" href="{{ route('employees.create') }}"> {{ __('site.Add New Employ')}}</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-bordered table-responsive">
                <tr>
                    <th>{{ __('site.No')}}</th>
                    <th>{{ __('site.First Name')}}</th>
                     <th>{{ __('site.Last Name')}}</th>
                    <th>{{ __('site.Email')}}</th>
                    <th>{{ __('site.Phone')}}</th>
                    <th>{{ __('site.Company')}}</th>
                    <th width="280px">{{ __('site.Action')}}</th>
                </tr>
                @foreach ($employees as $employ)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $employ->first_name }} </td>
                    <td> {{ $employ->last_name }}</td>
                    <td>{{ $employ->email }}</td>
                    <td>{{ $employ->phone }}</td>
                    <td>
                        <a class="txtlink" href="{{ route('companies.show',$employ->company_id) }}">{{App\Company::find( $employ->company_id)->name}}</a>
                    </td>
                    <td>
        
                        <a class="btn btn-info" href="{{ route('employees.show',$employ->id) }}">{{ __('site.Show')}}</a>
        
                        <a class="btn btn-primary" href="{{ route('employees.edit',$employ->id) }}">{{ __('site.Edit')}}</a>
                        <form action="{{ route('employees.destroy',$employ->id) }}" method="POST" style='display:inline'>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">{{ __('site.Delete')}}</button>
                        </form> 
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    {!! $employees->render() !!} 
</div>
@endsection