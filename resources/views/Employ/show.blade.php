@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
                <h2> {{ __('site.Show')}}  [{{ $employ->first_name }}  {{ $employ->last_name }} ]  </h2>
                 <hr>
	        </div>
	        
	    </div>
    </div>
   
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong> {{ __('site.First Name')}}:</strong>
               
                {{ $employ->first_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Last Name')}}:</strong>
              
                {{ $employ->last_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Email')}}:</strong>
                
                {{ $employ->email }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Phone')}}:</strong>
               
                {{ $employ->phone }}
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Company')}}:</strong>
                 <a class="txtlink" href="{{ route('companies.show',$employ->company_id) }}">{{App\Company::find( $employ->company_id)->name}}</a>
            </div>
        </div>
         
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" href="{{ route('employees.index') }}">{{ __('site.Back')}}</a>
        </div>
    </div>
</div>
@endsection