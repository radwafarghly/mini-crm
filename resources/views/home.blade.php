@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                
                <p>{{ __('site.Admin panel')}} {{ __('site.to manage')}} <a href="{{ route('companies.index') }}">{{ __('site.Companies')}}</a>.<br>
                 {{ __('site.Basically')}}, {{ __('site.a project to manage')}} <a href="{{ route('companies.index') }}">{{ __('site.Companies')}}</a> {{ __('site.and')}}  <a class="underline" href="{{ route('employees.index') }}">{{ __('site.Employees')}}</a>. Mini-CRM.</p>
                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
