@extends('layouts.app')
 
@section('content')
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="pull-left">
                <h2>{{ __('site.Companies')}}</h2>
                <hr>
            </div>
            <div class="pull-right text-md-right">
                <a class="btn btn-success" href="{{ route('companies.create') }}"> {{ __('site.Add New company')}}</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-bordered ">
                <tr>
                    <th>{{ __('site.No')}}</th>
                    <th width="80px">{{ __('site.Logo')}}</th>
                    <th>{{ __('site.Name')}}</th>
                    <th>{{ __('site.Email')}}</th>
                    <th width="280px">{{ __('site.Action')}}</th>
                </tr>
                @foreach ($companies as $company)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td><img alt="{{ $company->name }}" src="{{ asset('/storage/logo/'.$company->logo) }}"  class="rounded-circle " style="height: 60px;width: 36px;"></td>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->email }}</td>
                    <td>
                        <a class="btn btn-info" href="{{ route('companies.show',$company->id) }}">{{ __('site.Show')}}</a>
        
                        <a class="btn btn-primary" href="{{ route('companies.edit',$company->id) }}">{{ __('site.Edit')}}</a>
                        <form action="{{ route('companies.destroy',$company->id) }}" method="POST" style='display:inline'>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">{{ __('site.Delete')}}</button>
                        </form> 
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    {!! $companies->render() !!} 
</div>
@endsection