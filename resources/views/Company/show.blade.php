@extends('layouts.app')
 
@section('content')
<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
                <h2> {{ __('site.Show')}} [{{ $company->name }} ]  </h2>
                <hr>
	        </div>
	        
	    </div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Name')}}:</strong>
                {{ $company->name }}
            </div>
            <div class="form-group">
                <strong>{{ __('site.Email')}}:</strong>
                {{ $company->email }}
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Logo')}}:</strong>
                <img  alt="{{ $company->name }}" src="{{ asset('/storage/logo/'.$company->logo) }}"  class="rounded-circle" style="height: 134px;">
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" href="{{ route('companies.index') }}">{{ __('site.Back')}}</a>
        </div>
    </div>
</div>
@endsection