@extends('layouts.app')
 
@section('content')

<div class="container">
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
                <h2>{{ __('site.Edit')}}  [{{ $company->name }} ] </h2>
                <hr>
	        </div>
	    </div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Warning!!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    {!! Form::model($company, ['method' => 'PATCH','route' => ['companies.update', $company->id],'enctype'=>'multipart/form-data']) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Name')}}:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <strong>{{ __('site.Email')}}:</strong>
                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 p-3">
            <div class="form-group">
                <strong>{{ __('site.Logo')}}:</strong>
                {!! Form::file('logo', null, array('placeholder' => 'Logo','class' => 'form-control')) !!}
        </div>
         @isset($company->logo)
            <img alt="{{ $company->name }}" src="{{ asset('/storage/logo/'.$company->logo) }}"  class="rounded-circle " style="height: 134px;">
         @endisset

        
       {{-- </div> --}}
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">{{ __('site.Submit')}}</button>
                <a class="btn btn-primary" href="{{ route('companies.index') }}">{{ __('site.Back')}}</a>

        </div>
	</div>
    {!! Form::close() !!}
</div>
@endsection